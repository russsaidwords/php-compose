A simple scaffold for PHP projects using docker-compose.

To view a phpinfo(), simply run the project using `docker-compose up`, when all is stable load http://localhost in your browser.

Modify configurations for the docker containers in the `docker` directory.

Modify project code in the `project` directory.