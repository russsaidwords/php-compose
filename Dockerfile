FROM php:7.4-fpm

# Copy composer.lock and composer.json
COPY ./project/composer.lock ./project/composer.json /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies & clear cache
RUN apt-get update && apt-get install -y vim git curl && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
# RUN docker-php-ext-install pdo_mysql mbstring 

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY ./project/ /var/www

# Copy existing application directory permissions
COPY --chown=www:www ./project/ /var/www

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
